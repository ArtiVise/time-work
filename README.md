Учебный проект для Android. Приложение для записи времени работы сотрудников.
Функционал: Заполнение списка сотрудников через код. После выбора сотрудника можно заполнить время начала и конца работы на любую дату.
Для хранения данных используется SQLite.

Educational project for Android. Application to record the time of employees.
Functionality: Filling the list of employees through the code. After you select an employee, you can fill in the start and end times for any date.
SQLite is used for data storage.