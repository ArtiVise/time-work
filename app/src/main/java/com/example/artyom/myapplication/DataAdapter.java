package com.example.artyom.myapplication;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.artyom.myapplication.MainActivity.ID_WORKER;
import static com.example.artyom.myapplication.MainActivity.NAME_WORKER;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private List<Pair<Long, String>> workerList = new ArrayList<>();
    private Context context;

    DataAdapter(Context context, Cursor CursorWorkerList) {
        if (CursorWorkerList.moveToFirst()) {
            do {
                long id = CursorWorkerList.getInt(0);
                String name = CursorWorkerList.getString(1);
                workerList.add(Pair.create(id, name));
            }
            while (CursorWorkerList.moveToNext());
        }

        this.context = context;
    }

    @Override
    @NonNull
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.ViewHolder holder, int position) {
        Pair<Long, String> worker = workerList.get(position);
        holder.setText(worker);

    }

    @Override
    public int getItemCount() {
        return workerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameView;
        long id;

        ViewHolder(View view) {
            super(view);
            nameView = view.findViewById(R.id.name);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Main2Activity.class);
                    intent.putExtra(ID_WORKER, id);
                    intent.putExtra(NAME_WORKER, nameView.getText());
                    context.startActivity(intent);
                }
            });
        }

        void setText(Pair<Long, String> worker){
            nameView.setText(worker.second);
            id = worker.first;
        }

    }
}