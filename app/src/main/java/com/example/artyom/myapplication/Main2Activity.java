package com.example.artyom.myapplication;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.example.artyom.myapplication.MainActivity.ID_WORKER;
import static com.example.artyom.myapplication.MainActivity.NAME_WORKER;

public class Main2Activity extends AppCompatActivity {
    DatabaseAdapter adapter;
    DatabaseHelper databaseHelper;
    String selectedDate,name_worker;
    long id_worker;
    CalendarView calendarView;
    TextView TextStTime,TextFinTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        adapter = new DatabaseAdapter(this);
        //Получение полей времени начала и конца работы
        TextStTime = findViewById(R.id.textStTime);
        TextFinTime = findViewById(R.id.textFinTime);
        Bundle arguments = getIntent().getExtras();

        if(arguments!=null) {
            id_worker = arguments.getLong(ID_WORKER);
            name_worker = arguments.getString(NAME_WORKER);
            TextView TextView4 = findViewById(R.id.textView4);
            TextView4.setText(String.format("Работник %s", name_worker));
            //Toast.makeText(getApplicationContext(), String.valueOf(id_worker), Toast.LENGTH_LONG).show();
        }
        databaseHelper = DatabaseHelper.getInstance(this);

        calendarView = findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                //Получение выбранной даты и ее форматирование
                SimpleDateFormat  df = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                Calendar instCalendar = Calendar.getInstance();
                instCalendar.set(year,month,dayOfMonth);
                selectedDate = df.format(instCalendar.getTime());
                //Работа с БД
                adapter.open();
                Pair<String,String> time = adapter.getTime(id_worker,selectedDate);
                TextStTime.setText(time.first);
                TextFinTime.setText(time.second);
                adapter.close();
            }
        });
    }

    public void EditTime(View v) {
        int id = v.getId();
        TextView TextTime;
        switch (id) {
            case R.id.btChangeStartTime:
                // вызываем диалог с выбором времени начала работы
                TextTime = findViewById(R.id.textStTime);
                callEditTime(TextTime,"START_TIME");
                break;

            case R.id.btChangeFinishTime:
                // вызываем диалог с выбором времени окончания работы
                TextTime = findViewById(R.id.textFinTime);
                callEditTime(TextTime,"FINISH_TIME");
                //callEditTime2();
                break;
        }
    }
    private void callEditTime(final TextView TextTime, final String typeTime) {
        int mHour, mMinute;
        if(!TextTime.getText().equals(getResources().getString(R.string.textDefaultTime))) {
            String[] Time = TextTime.getText().toString().split(":");
            mHour = Integer.parseInt(Time[0]);
            mMinute = Integer.parseInt(Time[1]);
        }else{
            final Calendar cal = Calendar.getInstance();
            mHour = cal.get(Calendar.HOUR_OF_DAY);
            mMinute = cal.get(Calendar.MINUTE);
        }
        // инициализируем диалог выбора времени текущими значениями
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        //TextView TextStTime = findViewById(R.id.textStTime);
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        Calendar instCalendar = Calendar.getInstance();
                        instCalendar.set(0,0,0,hourOfDay,minute);
                        String Time = df.format(instCalendar.getTime());
                        TextTime.setText(Time);
                        adapter.open();
                        adapter.updateTime(id_worker,selectedDate,typeTime,Time);
                        adapter.close();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void onResume() {
        super.onResume();
        //Запрос текущей даты
        SimpleDateFormat  df = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        Calendar instCalendar = Calendar.getInstance();
        selectedDate = df.format(instCalendar.getTime());
        //Запрос в БД времени начала и конца работы на текущую дату
        adapter.open();
        Pair<String,String> time = adapter.getTime(id_worker,selectedDate);
        TextStTime.setText(time.first);
        TextFinTime.setText(time.second);
        adapter.close();
    }
}
