package com.example.artyom.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper helper;

    public static DatabaseHelper getInstance(Context context){
        if (helper == null){
            helper = new DatabaseHelper(context);
        }
        return helper;
    }

    private static final String DATABASE_NAME = "userstore.db"; // название бд
    private static final int SCHEMA = 1; // версия базы данных
    static final String TABLE = "workers"; // название таблицы в бд
    static final String TABLE2 = "timeworks"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_ID2 = "_id";
    public static final String ID_WORKER = "id_worker";
    public static final String DATE = "date";
    public static final String START_TIME = "starttime";
    public static final String FINISH_TIME = "fihishtime";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE workers (" + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NAME + " TEXT);");
        db.execSQL("CREATE TABLE timeworks (" + COLUMN_ID2
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + ID_WORKER + " INTEGER, " + DATE + " TEXT, " + START_TIME + " TEXT, " + FINISH_TIME  + " TEXT);");
        // добавление начальных данных
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Василий');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Марина');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Олег');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Ольга');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Наталья');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Павел');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Артем');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Никита');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Татьяна');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Арсен');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Виталий');", TABLE, COLUMN_NAME));
        db.execSQL(String.format("INSERT INTO %s (%s) VALUES ('Ксения');", TABLE, COLUMN_NAME));

        db.execSQL(String.format("INSERT INTO %s (%s,%s,%s,%s) VALUES (1,'04.10.2018','08:00','17:00');", TABLE2, ID_WORKER, DATE, START_TIME, FINISH_TIME));
        db.execSQL(String.format("INSERT INTO %s (%s,%s,%s,%s) VALUES (1,'05.10.2018','11:00','16:00');", TABLE2, ID_WORKER, DATE, START_TIME, FINISH_TIME));
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,  int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE);
        onCreate(db);
    }

}