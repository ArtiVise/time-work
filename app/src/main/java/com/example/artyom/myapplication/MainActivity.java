package com.example.artyom.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    public static final String ID_WORKER = "id_worker";
    public static final String NAME_WORKER = "name_worker";
    RecyclerView workerList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        workerList = findViewById(R.id.workersList);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        workerList.setLayoutManager(llm);
    }

    @Override
    public void onResume() {
        super.onResume();
        DatabaseAdapter adapter = new DatabaseAdapter(this);
        adapter.open();
        DataAdapter workerAdapter = new DataAdapter(this,adapter.getWorkerList());
        workerList.setAdapter(workerAdapter);
        adapter.close();
    }
}
