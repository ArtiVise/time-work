package com.example.artyom.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

public class DatabaseAdapter {
    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    public DatabaseAdapter(Context context){
        dbHelper = new DatabaseHelper(context.getApplicationContext());
    }

    public DatabaseAdapter open(){
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public Cursor getWorkerList(){
        return database.rawQuery("select * from " + DatabaseHelper.TABLE, null);
    }

    public Pair<String, String> getTime(long id_worker, String selectedDate){
        Pair<String, String> time;
        Cursor userCursor = database.rawQuery("select * from " + DatabaseHelper.TABLE2 + " where " + DatabaseHelper.DATE + " like ?" + " AND " + DatabaseHelper.ID_WORKER + " = " + id_worker , new  String[]{selectedDate});
        if (userCursor.moveToFirst()) {
            do {
                //int id = userCursor.getInt(0);
                //int id_wokrer = userCursor.getInt(1);
                //String date = userCursor.getString(2);
                String StTime = userCursor.getString(3);
                String FinTime = userCursor.getString(4);
                if(StTime == null){
                    StTime="Не задано";
                }
                if(FinTime == null){
                    FinTime="Не задано";
                }
                time=Pair.create(StTime,FinTime);
            }
            while (userCursor.moveToNext());
        }else{
            time=Pair.create("Не задано","Не задано");
        }
        userCursor.close();
        return time;

    }

    public void updateTime(long id_worker, String selectedDate,String TypeTime,String NewTime) {
        ContentValues cv = new ContentValues();
        if(TypeTime.equals("START_TIME")) {
            cv.put(DatabaseHelper.START_TIME, NewTime);
        }else {
            cv.put(DatabaseHelper.FINISH_TIME, NewTime);
        }
        long result = database.update(DatabaseHelper.TABLE2, cv, DatabaseHelper.DATE + " like ?" + " AND " + DatabaseHelper.ID_WORKER + " = " + id_worker, new String[]{selectedDate});
        if (result == 0) {
            cv.put(DatabaseHelper.ID_WORKER, id_worker);
            cv.put(DatabaseHelper.DATE, selectedDate);
            database.insert(DatabaseHelper.TABLE2, null, cv);
        }
        database.close();
    }
}
